import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/services/producto.service';
import { Producto } from 'src/app/models/producto.model';
import { Router } from '@angular/router';

@Component({
	selector: 'app-productos',
	templateUrl: './productos.component.html',
	styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {
	productos: Array<Producto> = [];

	constructor(
		public productoService: ProductoService,
		private router: Router
	) { }


	ngOnInit() {
		this.productoService.getProductos().subscribe(result => {
			this.productos = result;
		});
	}


	addProducto() {
		this.router.navigate(['/add-producto'])
	}

}
