'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var ProductoSchema = Schema({
    nombre: String
});

module.exports = mongoose.model('producto', ProductoSchema);
