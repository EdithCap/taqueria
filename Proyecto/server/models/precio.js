'use strict'


var PrecioSchema = Schema({
    valor: Number,
    fechaInicio: Date,
    fechaFin: Date,
    idProducto: {type: Schema.ObjectId, ref: 'producto'}
})

module.exports = mysql.module('precio', PrecioSchema);