'use strict'


var ProductoIngredienteSchema = Schema({
    cantidad: Number,
    idProducto: {type: Schema.ObjectId, ref: 'producto'},
    idIngrediente: {type: Schema.ObjectId, ref: 'ingrediente'}
})

module.exports = mysql.module('precio', ProductoIngredienteSchema);