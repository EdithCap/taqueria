'use strict'


var IngredienteSchema = Schema({
    nombre: String,
    bRefrigerado: Boolean,
    idUnidad: {type: Schema.ObjectId, ref: 'c_unidad_medida'}
})

module.exports = mysql.module('ingrediente', IngredienteSchema);