'use strict'

var express = require('express');
var ProductoController = require('../controllers/producto.controller');

var api = express.Router();

api.get('/productos', ProductoController.getProductos);
api.get('/producto/:productoId', ProductoController.getProduto);

api.post('/producto', ProductoController.save);

module.exports = api;