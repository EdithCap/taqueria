'use strict'

var Producto = require('../models/producto');

function getProductos(req, res) {
    Producto.find((err, productos) => {
        if (err) {
            return res.status(500).send({ message: 'Error en la petición' });
        } else {
            if (!productos)
                return res.status(400).send({ message: 'Error al obtener los registros' });

            console.log(productos);
            return res.status(200).send(productos);
        }
    })
}


function getProduto(req, res) {
    let params = req.body;
    let productoId = params.productoId;
    Producto.findById(productoId, (err, producto) => {
        if (err) {
            return res.status(500).send({ message: 'Error en la petición' });
        } else {
            if (!producto)
                return res.status(400).send({ message: 'Error al obtener el registro' });

            return res.status(200).send({ producto: producto });
        }
    });
}


function save(req, res) {
    var producto = new Producto();
    var params = req.body;
    console.log(params);

    producto.nombre = params.producto.nombre;
    producto.save((err, productoStored) => {
        if (err) {
            return res.status(500).send({ message: 'Error al guardar la tabla' });
        } else {
            return res.status(200).send({ producto: productoStored });
        }
    });
}


module.exports = {
    getProductos,
    getProduto,
    save,
}